package com.example.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Presenter {


    private static final long SCAN_PERIOD = 20 * 1000;



    private PresenterView view;
    private Context context;
    private HashMap<String, DeviceDTO> map;

    /**
     * Inicializa la clase
     *
     * @param view
     * @param context
     */
    public Presenter(PresenterView view, Context context) {
        this.view = view;
        this.context = context;
    }

    /**
     * al momento de pasar a background, se actualizan los datos para no tener excepciones
     */
    public void onPause() {
        view = null;
    }

    /**
     * si la actividad entra de background y no se inicializaron los datos, aqui se cargan
     *
     * @param view
     */
    public void onResume(PresenterView view) {
        if (this.view == null) {
            this.view = view;
        }
    }


    /**
     * verifica si el dispositivo tiene hardware de bluetooth
     */
    public void isBluetoothCompatible() {
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {


            if (view != null) {
                view.updateText("Bluetooth LE no soportado");
                view.enableView(R.id.btnSearch, false);
            }
        }
    }


    /**
     * Callback para leer los diferentes dispositivos que se han encontrado
     */
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            addItemToList(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);

        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            if (view != null) {
                view.updateText("Error escaneando dispositivos");
                view.progress(View.GONE);
            }
        }
    };


    /**
     * agrega cada dispositivo encontrado al listado de dispositivos, sin que se repitan por direccion MAC
     *
     * @param result
     */
    private void addItemToList(ScanResult result) {

        if (map == null) {
            map = new HashMap<>();

        }

        DeviceDTO item = new DeviceDTO();
        item.setName(result.getDevice().getName());
        item.setAddress(result.getDevice().getAddress());
        item.setRSSI(result.getRssi());
        item.setData(result);

        map.put(result.getDevice().getAddress(), item);


        List<DeviceDTO> devices = new ArrayList<>(map.values());


        if (view != null) {
            view.updateRecyclerView(devices);
        }
    }

    /**
     * metodo para inicializar la busqueda de dispositivos
     */
    public void startScanning() {

        if (view != null) {
            view.updateRecyclerView(new ArrayList<DeviceDTO>());
            view.enableView(R.id.btnSearch, false);
        }


        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            if (view != null) {
                view.updateText("Error, leyendo bluetooth");
                view.progress(View.GONE);
            }
            return;
        }

        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();


        //verify if bluetooth is enable if not,  enable it
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent in = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            if (view != null) {
                view.startActivitResult(in, MainActivity.REQUEST_SETTINGS_BT);
            }
            return;
        }


        final BluetoothLeScanner btScanner = bluetoothAdapter.getBluetoothLeScanner();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (view != null) {
                    view.updateText("Escaneo Completo");
                    view.progress(View.GONE);
                    view.enableView(R.id.btnSearch, true);
                }
                btScanner.stopScan(leScanCallback);
            }
        }, SCAN_PERIOD);

        if (view != null) {
            view.updateText("Escaneando...");
            view.progress(View.VISIBLE);
        }
        btScanner.startScan(leScanCallback);
    }


    public interface PresenterView {

        void updateText(String text);

        void enableView(int res, boolean enabled);

        void startActivitResult(Intent intent, int request);

        void updateRecyclerView(List<DeviceDTO> list);

        void progress(int visible);


    }


}
