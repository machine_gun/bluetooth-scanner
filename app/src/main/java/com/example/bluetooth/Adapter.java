package com.example.bluetooth;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private List<DeviceDTO> items;

    public Adapter(List<DeviceDTO> items) {
        this.items = items;
    }

    public void updateData(List<DeviceDTO> items) {
        this.items = items;
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DeviceDTO item = items.get(position);

        holder.tvName.setText(item.getName());
        holder.tvAddress.setText(item.getAddress());
        String tmp = item.getRSSI() + "";
        holder.tvRSSI.setText(tmp);
        holder.row.setTag(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View row;
        private TextView tvName;
        private TextView tvAddress;
        private TextView tvRSSI;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            row = itemView.findViewById(R.id.row);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvRSSI = itemView.findViewById(R.id.tvRSSI);
            //  row.setOnClickListener(this);
        }

        //  @Override
        //  public void onClick(View v) {
        //      DeviceDTO item = (DeviceDTO) v.getTag();
        //      Log.i("", "onClick: ");
        //  }
    }
}
