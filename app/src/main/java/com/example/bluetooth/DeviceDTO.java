package com.example.bluetooth;

import android.bluetooth.le.ScanResult;

public class DeviceDTO {
    private String name;
    private String address;
    private int RSSI;
    private ScanResult data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRSSI() {
        return RSSI;
    }

    public void setRSSI(int RSSI) {
        this.RSSI = RSSI;
    }

    public ScanResult getData() {
        return data;
    }

    public void setData(ScanResult data) {
        this.data = data;
    }
}
