package com.example.bluetooth;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Presenter.PresenterView {


    private Presenter presenter;
    public static final int REQUEST_SETTINGS_BT = 4;
    public static final int REQUEST_PERMISSIONS = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       askPermissions();

        presenter = new Presenter(this, this);


        presenter.isBluetoothCompatible();
        initViews();


    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
       presenter.onResume(this);
    }

    private void initViews() {

        findViewById(R.id.btnSearch).setOnClickListener(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SETTINGS_BT:
                presenter.startScanning();
                break;
        }
    }

    private void askPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {


                    Toast.makeText(MainActivity.this, "Los permisos son necesarios para continuar", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might requ
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearch:
                presenter.startScanning();
                break;
        }
    }

    @Override
    public void updateText(String info) {
        ((TextView) findViewById(R.id.tvStatus)).setText(info);
    }

    @Override
    public void enableView(int res, boolean enabled) {
        findViewById(res).setEnabled(enabled);
    }

    @Override
    public void startActivitResult(Intent intent, int request) {
        startActivityForResult(intent, request);
    }

    @Override
    public void updateRecyclerView(List<DeviceDTO> list) {
        RecyclerView rv = findViewById(R.id.rv);

        Adapter adapter;
        if(rv.getAdapter() == null){
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            rv.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),layoutManager.getOrientation());
            rv.addItemDecoration(dividerItemDecoration);
            adapter = new Adapter(list);
            rv.setAdapter(adapter);
        }else{
            adapter = (Adapter) rv.getAdapter();
            adapter.updateData(list);
        }



    }

    @Override
    public void progress(int visible) {
        ProgressBar pb = findViewById(R.id.pb);
        pb.setVisibility(visible);
    }
}